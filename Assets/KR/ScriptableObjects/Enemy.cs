using UnityEngine;

[CreateAssetMenu(fileName = "Enemy", menuName = "KR/Enemy", order = 1)]
public class Enemy : ScriptableObject
{
    public Status status;
}
