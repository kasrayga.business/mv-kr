using UnityEngine;

[CreateAssetMenu(fileName = "Tower", menuName = "KR/Tower", order = 1)]
public class Tower : ScriptableObject
{
    [SerializeField] private Sprite sprite; 

    public Sprite Sprite => sprite;
    public Status status;
}
