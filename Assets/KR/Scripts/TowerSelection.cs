using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerSelection : MonoBehaviour
{
    private GameManager GameManager => GameManager.Instance;
    private bool onSelection = false;
    [SerializeField] private SpriteRenderer towerSelected;
    private TowerSelectionUI towerSelectionUI;
    public TowerSelectionUI TowerSelectionUI => towerSelectionUI;

    private void InitGetComponents()
    {
        GameManager.TowerSelection = this;
        towerSelectionUI = GetComponent<TowerSelectionUI>();
    }

    private void Awake()
    {
        InitGetComponents();
    }

    public void SelectTower(int index)
    {
        onSelection = true;
        towerSelected.gameObject.SetActive(true);

        GameManager.TowerSelectedIndex = index;
        towerSelected.sprite = GameManager.TowerData[index].Data.Sprite;

        GameManager.ShowAvailableTowers();
    }

    private void Update()
    {
        if(onSelection)
        {
            Vector3 mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            mousePosition.y = 0f;
            towerSelected.transform.position = mousePosition;
        }
        
        if(Input.GetMouseButtonDown(0))
        {
            onSelection = false;
            towerSelected.gameObject.SetActive(false);
            GameManager.ShowAvailableTowers(false);
        }
        else if (Input.GetMouseButtonDown(1) || Input.GetMouseButtonDown(2))
        {
            onSelection = false;
            towerSelected.gameObject.SetActive(false);
            GameManager.ShowAvailableTowers(false);
            GameManager.TowerSelectedIndex = -1;
        }
    }
}
