using UnityEngine;

public class EnemyObjective : MonoBehaviour
{
    [SerializeField]
    private Transform nextObjective;
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Enemy"))
        {
            other.GetComponent<EnemyController>().AgentObjective = nextObjective;
        }
    }
}
