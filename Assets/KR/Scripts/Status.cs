using UnityEngine;

[System.Serializable]
public class Status
{
    [SerializeField] private int level;
    [SerializeField] private float health, attackPoint, attackSpeed, range;

    public int Level => level;
    public float Health
    {
        get { return health; }
        set { health = value; }
    }
    public float AttackPoint => attackPoint;
    public float AttackSpeed => attackSpeed;
    public float Range => range;

    public Status (Status status)
    {
        this.level = status.level;
        this.health = status.health;
        this.attackPoint = status.attackPoint;
        this.attackSpeed = status.attackSpeed;
        this.range = status.range;
    }
}
