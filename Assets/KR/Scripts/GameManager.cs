using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public enum GameStatus {
    Play,
    Pause,
    GameOver
}

[System.Serializable]
public class TowerData {
    [SerializeField] private Tower data;
    [SerializeField] private int amountInit;
    public Tower Data => data;
    public int AmountInit => amountInit;
    public int Amount { get; set; } = 0;
    public int GetRealAmount => AmountInit - Amount;
    public bool IsAbleToInstance => !GetRealAmount.Equals(0);
}

public class GameManager : Singleton<GameManager>
{
    private GameStatus gameStatus = GameStatus.Play;

    [SerializeField] private TowerData[] towerData;
    public TowerData[] TowerData => towerData;
    public int TowerSelectedIndex { get; set; } = -1;
    public TowerData GetSelectedTowerData => towerData[TowerSelectedIndex];

    public TowerSelection TowerSelection { get; set; }

    [SerializeField] private List<TowerInstance> towerInstances = new();

    [SerializeField] private Transform enemyObjectiveInit;
    public Transform EnemyObjectiveInit => enemyObjectiveInit;
    public Transform EnemyObjective { get; set; }

    [SerializeField] private int scoreTime = 5;
    private int score;
    public int Score => score;

    protected override void Awake()
    {
        base.Awake();
        InvokeRepeating(nameof(InvokeScore), scoreTime, scoreTime);
    }

    public void RestartGame()
    {
        print(SceneManager.GetActiveScene().buildIndex);
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    private void InvokeScore()
    {
        score += scoreTime;
    }

    public void AddTowerInstance (TowerInstance towerInstance)
    {
        if(!towerInstances.Contains(towerInstance))
        {
            towerInstances.Add(towerInstance);
        }
    }

    public void ShowAvailableTowers()
    {
        foreach (TowerInstance i in towerInstances)
        {
            i.ShowAvailable();
        }
    }

    public void ShowAvailableTowers(bool status)
    {
        foreach (TowerInstance i in towerInstances)
        {
            i.ShowAvailable(status);
        }
    }

    public bool IsGameInPlay => gameStatus.Equals(GameStatus.Play);
    public bool IsGameInPause => gameStatus.Equals(GameStatus.Pause);
    public bool IsGameOver => gameStatus.Equals(GameStatus.GameOver);

    public void DoGameOver()
    {
        gameStatus = GameStatus.GameOver;
        GameStatusUI.Instance.StopGame();
    }
}
