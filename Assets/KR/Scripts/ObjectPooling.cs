using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPooling : Singleton<ObjectPooling>
{
    private GameManager GameManager => GameManager.Instance;

    [SerializeField] private int amountOfEnemies = 10;
    [SerializeField] private EnemyController enemyPrefab;
    public List<EnemyController> EnemyInstances { get; set; } = new();

    [SerializeField] private int amountOfProjectiles = 20;
    [SerializeField] private Projectile projectilePrefab;
    public List<Projectile> ProjectileInstances { get; set; } = new();

    [SerializeField] private Transform[] spawnTransforms;

    [SerializeField] private int[] stagesDifficultyTimes;
    [SerializeField] private int[] stagesSpawnTime;
    [SerializeField] private int[] stagesMaxConcurrent;


    private void Start()
    {
        for (int i = 0; i < amountOfEnemies; i++)
        {
            EnemyInstances.Add(Instantiate(enemyPrefab).GetComponent<EnemyController>());
            EnemyInstances[^1].gameObject.SetActive(false);
        }

        for (int i = 0; i < amountOfProjectiles; i++)
        {
            ProjectileInstances.Add(Instantiate(projectilePrefab).GetComponent<Projectile>());
            ProjectileInstances[^1].gameObject.SetActive(false);
        }
        //InvokeRepeating(nameof(EndlessEnemySpawn), 3f, spawnCooldown);
    }

    public void StartGame()
    {
        StartCoroutine(EndlessSpawn());
    }

    public void StopGame()
    {
        StopCoroutine(EndlessSpawn());
    }

    private IEnumerator EndlessSpawn()
    {
        int i = 0;
        int index = 0;
        while(!GameManager.IsGameOver)
        {
            if (index < (stagesDifficultyTimes.Length - 1) && i > stagesDifficultyTimes[index])
            {
                index++;
                print(index);
            }

            yield return new WaitForSeconds(stagesSpawnTime[index]);

            if (GetActiveEnemies() < stagesMaxConcurrent[index])
            {
                InstanceEnemy();
            }
            i++;
        }
    }

    public void InstanceEnemy()
    {
        InstanceEnemy(spawnTransforms[Random.Range(0, spawnTransforms.Length - 1)].position);
    }

    public void InstanceEnemy(Vector3 initPosition)
    {
        foreach (EnemyController i in EnemyInstances)
        {
            if (!i.gameObject.activeInHierarchy)
            {
                i.transform.position = initPosition;
                i.gameObject.SetActive(true);
                return;
            }
        }
    }

    public void InstanceProjectile(Vector3 position, float attackPoint)
    {
        foreach(Projectile i in ProjectileInstances)
        {
            if (!i.gameObject.activeInHierarchy)
            {
                try
                {
                    i.transform.position = position;
                    i.TargetTransform = GetClosestEnemy(position);
                    i.Damage = attackPoint;
                    i.gameObject.SetActive(true);
                } catch (System.NullReferenceException)
                {
                    return;
                }
                return;
            }
        }
    }

    public int GetActiveEnemies()
    {
        int sum = 0;
        foreach (EnemyController i in EnemyInstances)
        {
            if (i.gameObject.activeInHierarchy)
            {
                sum++;
            }
        }
        return sum;
    }

    public Transform GetClosestEnemy(Vector3 position)
    {
        EnemyController temp = null;
        foreach (EnemyController i in EnemyInstances)
        {
            if (i.gameObject.activeInHierarchy)
            {
                if(!temp)
                {
                    temp = i;
                }
                if(Vector3.Distance(i.transform.position, position) < 
                    Vector3.Distance(temp.transform.position, position))
                {
                    temp = i;
                }
            }
        }
        return temp.transform;
    }

    public float GetRangeToClosestEnemy(Vector3 fromPosition)
    {
        return Vector3.Distance(fromPosition, GetClosestEnemy(fromPosition).position);
    }
}
