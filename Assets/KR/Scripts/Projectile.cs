using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    [SerializeField] private float speed = 15f;

    public Transform TargetTransform { get; set; }
    public float Damage { get; set; }

    private void Update()
    {
        transform.position = Vector3.MoveTowards(transform.position, TargetTransform.position, speed * Time.deltaTime);
        if(!TargetTransform.gameObject.activeInHierarchy)
        {
            gameObject.SetActive(false);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        try
        {
            EnemyController target = null;
            if (other.CompareTag("Enemy") && other.gameObject.activeInHierarchy)
            {
                target = other.GetComponent<EnemyController>();
            }

            target.Status.Health -= Damage;
            target.UpdateUIHealth();

            if (target.Status.Health <= 0f)
            {
                target.Disable();
                //enemyController.AgentTarget = null;
            }

            gameObject.SetActive(false);
        } catch (System.NullReferenceException)
        {

        }
    }
}
