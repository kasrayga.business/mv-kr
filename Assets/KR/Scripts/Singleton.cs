//KR Singleton baseclass
using UnityEngine;

public class Singleton<T> : MonoBehaviour where T : Component
{
	private static T instance;
	public static T Instance => instance;

	protected virtual void Awake()
	{
		if (!instance)
		{
			instance = this as T;
		}
		else
		{
			Destroy(gameObject);
		}
	}
}

public class SingletonDontDestroyOnLoad<T> : MonoBehaviour where T : Component
{
	private static T instance;
	public static T Instance => instance;

	protected virtual void Awake()
	{
		if (!instance)
		{
			instance = this as T;
			//Debug.Log(instance.name + " assigned as singleton and no destroy on load");
			DontDestroyOnLoad(this.gameObject);
		}
		else
		{
			Debug.Log(name + " being destroyed as duplicated singleton");
			Destroy(gameObject);
		}
	}
}