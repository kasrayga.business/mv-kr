using UnityEngine;
using UnityEngine.UI;

public class TowerSelectionUI : MonoBehaviour
{
    private GameManager GameManager => GameManager.Instance;

    [SerializeField] private Button[] button;
    [SerializeField] private Image[] iconImage;
    [SerializeField] private TMPro.TextMeshProUGUI[] healthText;
    [SerializeField] private TMPro.TextMeshProUGUI[] attackPointText;
    [SerializeField] private TMPro.TextMeshProUGUI[] attackSpeedText;
    [SerializeField] private TMPro.TextMeshProUGUI[] rangeText;
    [SerializeField] private TMPro.TextMeshProUGUI[] amountTower;

    private void Awake()
    {
        for(int i = 0; i < iconImage.Length; i++)
        {
            iconImage[i].sprite = GameManager.TowerData[i].Data.Sprite;
            healthText[i].text = GameManager.TowerData[i].Data.status.Health.ToString("F0");
            attackPointText[i].text = GameManager.TowerData[i].Data.status.AttackPoint.ToString("F0");
            attackSpeedText[i].text = GameManager.TowerData[i].Data.status.AttackSpeed.ToString("F0");
            rangeText[i].text = GameManager.TowerData[i].Data.status.Range.ToString("F0");
            amountTower[i].text = GameManager.TowerData[i].GetRealAmount.ToString("F0");
        }
    }

    public void UpdateUIAmount(int index)
    {
        amountTower[index].text = GameManager.TowerData[index].GetRealAmount.ToString("F0");
        if(GameManager.TowerData[index].IsAbleToInstance)
        {
            button[index].interactable = true;
        }
        else
        {            
            button[index].interactable = false;
        }
    }
}
