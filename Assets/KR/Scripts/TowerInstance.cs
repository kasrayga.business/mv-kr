using UnityEngine;

public class TowerInstance : MonoBehaviour
{
    private GameManager GameManager => GameManager.Instance;
    private ObjectPooling ObjectPooling => ObjectPooling.Instance;
    private TowerSelectionUI TowerSelectionUI => GameManager.TowerSelection.TowerSelectionUI;

    private bool isAvailable = false;
    public bool IsAvailable => isAvailable;

    [SerializeField] private bool isAncient;
    [SerializeField] private UnityEngine.UI.Slider healthBar;

    private SpriteRenderer spriteRenderer;
    [SerializeField] private SpriteRenderer spriteRendererAvail;

    [SerializeField] private Status status;
    public Status Status => status;
    private Status statusDefault;

    private bool onEnter = false;
    private SphereCollider thisCollider;

    private float attackTimer = 10f;

    public void UpdateUIAmount()
    {
        TowerSelectionUI.UpdateUIAmount(GameManager.TowerSelectedIndex);
    }

    public void UpdateUIHealth()
    {
        healthBar.maxValue = statusDefault.Health;
        healthBar.minValue = 0f;
        healthBar.value = status.Health;
    }

    private void InitGetComponents()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
        thisCollider = GetComponent<SphereCollider>();
    }

    private void Awake()
    {
        InitGetComponents();

        if (!isAncient)
        {
            isAvailable = true;
            DisableTower();
        }
        else
        {
            statusDefault = new Status(status);
            UpdateUIHealth();
        }

        //GameManager.AddTowerInstance(this);
    }

    public void DisableTower()
    {
        if(isAncient)
        {
            GameManager.DoGameOver();
        }

        if(!GameManager.TowerSelectedIndex.Equals(-1))
        {
            ShowAvailable(true);
        }
        else
        {
            ShowAvailable(false);

        }

        onEnter = false;
        isAvailable = true;
        thisCollider.enabled = false;
        spriteRenderer.sprite = null;
        healthBar.gameObject.SetActive(false);
    }

    public void ShowAvailable()
    {
        try
        {
            spriteRendererAvail.gameObject.SetActive(IsAvailable);
        }
        catch (UnassignedReferenceException)
        {

        }
    }

    public void ShowAvailable(bool status)
    {
        try
        {
            spriteRendererAvail.gameObject.SetActive(status);
        }
        catch (UnassignedReferenceException)
        {

        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Tower"))
        {
            onEnter = true;
        }
        else if (other.CompareTag("Enemy"))
        {
            other.GetComponent<EnemyController>().AgentTarget = transform;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Tower"))
        {
            onEnter = false;
        }
    }

    private void FixedUpdate()
    {
        if (onEnter && Input.GetMouseButton(0))
        {
            Assign();
        }
    }

    private void Update()
    {
        if(!isAvailable)
        {
            attackTimer += Time.deltaTime;
            try
            {
                if (attackTimer >= 10f &&
                ObjectPooling.GetRangeToClosestEnemy(transform.position) <= status.Range)
                {
                    ObjectPooling.InstanceProjectile(transform.position, status.AttackPoint);
                    attackTimer = status.AttackSpeed;
                }
            } catch (System.NullReferenceException)
            {

            }
        }
    }

    public void Assign()
    {
        if(isAvailable && GameManager.GetSelectedTowerData.IsAbleToInstance)
        {
            spriteRenderer.sprite = GameManager.GetSelectedTowerData.Data.Sprite;
            statusDefault = new Status(GameManager.GetSelectedTowerData.Data.status);
            status = new Status(statusDefault);

            GameManager.ShowAvailableTowers(false);
            isAvailable = false;
            thisCollider.enabled = true;
            healthBar.gameObject.SetActive(true);
            GameManager.GetSelectedTowerData.Amount++;

            UpdateUIHealth(); 
            UpdateUIAmount();

            GameManager.TowerSelectedIndex = -1;
        }
    }
}
