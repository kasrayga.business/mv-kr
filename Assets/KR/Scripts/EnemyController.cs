using UnityEngine;
using UnityEngine.AI;

public class EnemyController : MonoBehaviour
{
    private static int layerOrder = 0;
    private GameManager GameManager => GameManager.Instance;
    private NavMeshAgent agent;
    [SerializeField] private SpriteRenderer spriteRenderer;
    [SerializeField] private EnemyAnimator thisAnimator;
    [SerializeField]
    private Enemy enemyBase;
    private Status status;
    public Status Status => status;
    private Status statusDefault;

    public Transform AgentTarget { get; set; }
    
    public Transform AgentObjective { get; set; }

    [SerializeField] private UnityEngine.UI.Slider healthBar;

    private float attackTimer = 10f;

    public void UpdateUIHealth()
    {
        healthBar.maxValue = statusDefault.Health;
        healthBar.minValue = 0f;
        healthBar.value = status.Health;
    }

    private void OnEnable()
    {
        Awake();
        spriteRenderer.sortingOrder = layerOrder;
        layerOrder++;
    }

    public void Disable()
    {
        gameObject.SetActive(false);
    }

    private void Awake()
    {
        statusDefault = new Status(enemyBase.status);
        status = new Status(statusDefault);
        AgentTarget = null;
        AgentObjective = GameManager.EnemyObjectiveInit;
        agent = GetComponent<NavMeshAgent>();
        agent.updateRotation = false;
        UpdateUIHealth();
    }

    private void Update()
    {
        if(GameManager.IsGameOver)
        {
            thisAnimator.Idle();
            agent.SetDestination(transform.position);
            return;
        }

        if(AgentTarget)
        {
            attackTimer += Time.deltaTime;
            agent.SetDestination(AgentTarget.position);
            if (DistanceToTarget >= status.Range)
            {
                thisAnimator.Walk();
            }
            else if (attackTimer >= 10f)
            {
                thisAnimator.Attack();
                attackTimer = status.AttackSpeed;
            }
            else
            {
                thisAnimator.Idle();
                agent.SetDestination(transform.position);
            }
        }
        else if(AgentObjective)
        {
            agent.SetDestination(AgentObjective.position);
            if (DistanceToObjective >= status.Range)
            {
                thisAnimator.Walk();
            }
            else
            {
                thisAnimator.Idle();
                agent.SetDestination(transform.position);
            }
        }
        else
        {
            thisAnimator.Idle();
            agent.SetDestination(transform.position);
        }
    }

    private float DistanceToTarget => Vector3.Distance(AgentTarget.position, transform.position);
    private float DistanceToObjective => Vector3.Distance(AgentObjective.position, transform.position);
}

