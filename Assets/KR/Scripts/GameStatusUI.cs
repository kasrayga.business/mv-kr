using UnityEngine;
using UnityEngine.UI;

public class GameStatusUI : Singleton<GameStatusUI>
{
    private GameManager GameManager => GameManager.Instance;

    [SerializeField] private GameObject panelStatus;
    [SerializeField] private TMPro.TextMeshProUGUI scoreText;
    private readonly string scoreBase = "Your Score is ";

    [SerializeField] private GameObject startButton;
    [SerializeField] private GameObject reStartButton;

    protected override void Awake()
    {
        base.Awake();
        scoreText.gameObject.SetActive(false);
        reStartButton.SetActive(false);
    }

    public void EnablePanel()
    {
        scoreText.gameObject.SetActive(true);
        panelStatus.SetActive(true);
        startButton.SetActive(false);
        reStartButton.SetActive(true);
    }

    public void DisablePanel()
    {
        scoreText.gameObject.SetActive(false);
        panelStatus.SetActive(false);
    }

    public void StartGame()
    {
        ObjectPooling.Instance.StartGame();
        DisablePanel();
    }

    public void ReStartGame()
    {
        GameManager.Instance.RestartGame();
        DisablePanel();
    }

    public void StopGame()
    {
        UpdateUIScore();
        EnablePanel();
        ObjectPooling.Instance.StopGame();
    }

    private void UpdateUIScore()
    {
        scoreText.text = scoreBase + GameManager.Score;
    }
}
