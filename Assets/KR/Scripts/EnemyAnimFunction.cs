using UnityEngine;

public class EnemyAnimFunction : MonoBehaviour
{
    private EnemyController enemyController;
    private TowerInstance target;

    private void Awake()
    {
        enemyController = GetComponentInParent<EnemyController>();
    }

    private void AttackDeal()
    {
        try
        {
            target = enemyController.AgentTarget.GetComponent<TowerInstance>();
            target.Status.Health -= enemyController.Status.AttackPoint;
            target.UpdateUIHealth();

            if (target.Status.Health <= 0f)
            {
                target.DisableTower();
                enemyController.AgentTarget = null;
            }
        } catch (System.NullReferenceException)
        {

        }
    }
}
