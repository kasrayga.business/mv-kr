using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAnimator : MonoBehaviour
{
    private Animator anim;

    private void Awake()
    {
        anim = GetComponent<Animator>();
    }

    public void Idle()
    {
        anim.SetBool("Idle", true);
        anim.SetBool("Walk", false);
        anim.SetBool("Attack", false);
    }

    public void Walk()
    {
        anim.SetBool("Walk", true);
        anim.SetBool("Idle", false);
        anim.SetBool("Attack", false);
    }

    public void Attack()
    {
        anim.SetTrigger("Attack");
    }

    public void Die()
    {
        anim.SetTrigger("Die");
    }
}
